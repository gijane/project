<?php

include 'Book.php';

class Library {


    public function countBook($books) {
        return count($books);
    }

    public function getBooksCountByYear($books, $year){

        $count = 0;

        foreach ($books as $book){
            if($book->getYear() == $year){
                $count++;
            }
        }
        return $count;
    }
}