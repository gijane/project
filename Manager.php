<?php
include 'Library.php';

$library = new Library();
$books = array(
    new Book('Jane Eyre', 1860),
    new Book ('Tom Sawyer', 1830),
    new Book('The Lightning Thief', 2005),
);

echo 'Year 1860: ', $library->getBooksCountByYear($books,1860), "\n";
echo 'Count: ', $library->countBook($books), "\n";