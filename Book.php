<?php


class Book {

    private $title;
    private $year;

    /**
     * Book constructor.
     * @param $title
     * @param $year
     */
    public function __construct($title, $year) {
        $this->title = $title;
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year) {
        $this->year = $year;
    }
}