<?php

include 'Library.php';

use  \PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testYearCount()
    {
        $books = array(
            new Book('Jane Eyre', 1860),
            new Book ('Tom Sawyer', 1830),
            new Book('The Lightning Thief', 2005),
            new Book('The Lightning Thief', 2005),
            new Book('The Lightning Thief', 2005));
        $year = new Library;
        $result = $year->getBooksCountByYear($books, 2005);
        $this->assertEquals(3, $result);
    }

    /**
     * @dataProvider countDataProvider
     * @param $actual
     * @param $expected
     */

    public function testCount($books, $expected)
    {
        $result = (new Library)->countBook($books);
        $this->assertEquals($expected, $result);
    }

    public function countDataProvider()
    {
        return array(
            array(
                array(
                    new Book('Jane Eyre', 1860),
                    new Book ('Tom Sawyer', 1830),
                    new Book('The Lightning Thief', 2005),
                ), 32),
        );
    }
}